# Annotator

Web tool for annotating images for use in machine learning training sets.
This repo was forked from  the LOST labeler at [l3p-cv/lost](https://github.com/l3p-cv/lost).

1. [View LOST Github repo](https://github.com/l3p-cv/lost).
2. [View LOST documentation](https://lost.readthedocs.io/en/latest/).

This version of the LOST project focuses only on simple, single-image, bounding-box annotations and disregards the complex-shape, multi-image, and/or semi-automatic labelling tools available in the original project. As such, some dependencies essential for the original project have been omitted to consume fewer resources.

## Contents

1. [Deploy](#deploy)
2. [Roll Back](#roll-back)
3. [Dev Environment Setup](#dev-environment-setup)

## Deploy

This annotator tool utilizes Docker and Systemd to be set up as a system service. Please view [instructions for setting up the annotator with systemd](https://gitlab.com/byuhbll/apps/backfile-annotator).

## Roll Back

...

## Dev Environment Setup

...