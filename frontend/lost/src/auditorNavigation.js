export default {
  items: [
    {
      name: 'Overview',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Tasks',
      url: '/tasks',
      icon: 'fa fa-tasks'
    },
  ],
}
