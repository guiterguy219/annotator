import React from 'react'
import Loadable from 'react-loadable'

function Loading() {
  return <div>Loading...</div>
}

const Dashboard = Loadable({
  loader: () => import('./views/AuditorDashboard.js'),
  loading: Loading,
});

const Tasks = Loadable({
  loader: () => import('./views/AuditorTasks.js'),
  loading: Loading,
});

const routes = [
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/tasks', name: 'Tasks', component: Tasks },
]

export default routes
