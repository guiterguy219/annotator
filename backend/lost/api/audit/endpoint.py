from flask import request
from flask_restplus import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from lost.api.api import api
from lost.api.audit.res_marsh_definition import user_tasks
from lost.api.label.api_definition import label_trees
from lost.db import roles, access
from lost.settings import LOST_CONFIG, DATA_URL
from lost.logic import audit
import json

namespace = api.namespace('sia', description='SIA Annotation API.')

@namespace.route('/tasks/<string:user_id>')
@namespace.param('user_id', 'The id of user to be audited.')
class Get_User_Tasks(Resource):
    # @api.marshal_with(user_tasks)
    @jwt_required 
    def get(self):
        dbm = access.DBMan(LOST_CONFIG)
        identity = get_jwt_identity()
        user = dbm.get_user_by_id(identity)
        if not user.has_role(roles.AUDITOR):
            dbm.close_session()
            return "You need to be {} in order to perform this request.".format(roles.AUDITOR), 401
        else:
            re = audit.get_user_tasks(dbm, identity, int(user_id), DATA_URL)
            dbm.close_session()
            return re
