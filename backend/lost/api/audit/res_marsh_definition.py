from flask_restplus import fields

from lost.api.api import api

user_tasks = api.model('User Tasks', {
    'user_id': fields.Integer(readOnly=True, description='User identifier.'),
    ...
})
